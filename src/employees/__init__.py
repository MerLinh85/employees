from flask import Flask
from .webcontrollers import employeesweb
from .restcontrollers import employeesrest
from . import repo
import os

app = Flask(__name__)

DATABASE_ENABLED = bool(os.environ.get("DATABASE_ENABLED", False))
app.config['DATABASE_ENABLED'] = DATABASE_ENABLED
app.logger.info(f"Database ENABLED: {DATABASE_ENABLED}")

if DATABASE_ENABLED:
    DEFAULT_DATABASE_HOST = 'localhost'
    DATABASE_HOST = os.environ.get("DATABASE_HOST", DEFAULT_DATABASE_HOST)
    app.logger.info(f"Database HOST: {DATABASE_HOST}")
    app.config['DATABASE_HOST'] = DATABASE_HOST

    with app.app_context():
        repo.init()

# RuntimeError: The session is unavailable because no secret key was set.  Set the secret_key on the application to something unique and secret.
# sessionkezelés miatt
app.config['SECRET_KEY'] = "employees"

app.register_blueprint(employeesweb)
app.register_blueprint(employeesrest)


