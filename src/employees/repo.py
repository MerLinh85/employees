import mysql.connector
from flask import current_app

def sql(funct):
    def wrap_function(*args, **kwargs):
        try:
            host = current_app.config.get("DATABASE_HOST")
            with mysql.connector.connect(user='employees', password='employees', host=host, database='employees', use_pure=False) as conn:
                kwargs['conn'] = conn
                return funct(*args, **kwargs)
        except mysql.connector.Error as e:
            current_app.logger.error(e)    
                    
    return wrap_function

@sql
def init(conn = None):
    with conn.cursor() as cursor:
        cursor.execute(
            "create table if not exists employees (id bigint not null auto_increment, emp_name varchar(255), primary key (id))")

                        
@sql
def find_all(conn = None):
    with conn.cursor(buffered=True) as cursor:
        cursor.execute("select id, emp_name from employees")
        employees = []
        for (id, name) in cursor:
             employees.append({"id": id, "name": name})
        return employees

@sql
def save(command, conn = None):
    with conn.cursor() as cursor:
        cursor.execute("insert into employees(emp_name) values (%s)", (command["name"],))
        conn.commit()
        return {"id": cursor.lastrowid}

@sql
def delete_all(conn = None):
    with conn.cursor() as cursor:
        cursor.execute("delete from employees")
        conn.commit()