from employees.services import get_message

def test_get_message():
    assert "Hello from Python" in get_message()